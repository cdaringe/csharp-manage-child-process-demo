# csharp-manage-child-process-demo

## requirements

- mono/.net 4.x+
- nodejs 6.x+

## install

- clone it!
- cd to it!
- `npm install -g process-widget`

## usage

see `ChildProcessHandles.csproj`.  build it.  run it.