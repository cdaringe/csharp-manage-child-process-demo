﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ChildProcessHandles
{
	class MainClass
	{
		public static Process cp;
		public static bool hasExited = false;
		public static string cpInfoFilename = Path.Combine(Process.GetCurrentProcess().StartInfo.WorkingDirectory, "child-process-info.json");

		public static void onParentProcessExit()
		{
			if (hasExited) return;
			hasExited = true;
			Console.WriteLine("MAIN PROCESS EXITING");
			if (!cp.HasExited)
			{
				cp.Kill();
			}
		}
		public static void Main(string[] args)
		{
			Console.WriteLine("MAIN PROCESS STARTED");
			bootChild();
			Thread.GetDomain().ProcessExit += (sender, e) =>
			{
				onParentProcessExit();
			};
			Console.CancelKeyPress += (sender, e) => onParentProcessExit();
			while (true)
			{
				Thread.Sleep(60000);
				Environment.Exit(0);
			}
		}
		public static void bootChild()
		{
			Process mainProcess = Process.GetCurrentProcess();
			cp = new Process();
			cp.StartInfo.CreateNoWindow = true;
			cp.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			cp.StartInfo.UseShellExecute = false;
			cp.StartInfo.RedirectStandardError = true;
			cp.StartInfo.RedirectStandardOutput = true;
			cp.StartInfo.FileName = "process-widget";
			cp.StartInfo.Arguments = "run"; // run-graceful-exit, run-crash
			cp.EnableRaisingEvents = true;
			cp.Exited += (sender, e) =>
			{
				Console.WriteLine("CHILD PROCESS EXITING");
				File.Delete(cpInfoFilename);
				// if (((Process)sender).ExitCode != 0) mainProcess.Kill();
				// ^ if you only want to kill the parent if the child failed
				mainProcess.Kill();
				// ^ if you only want to kill the parent on child exit always!
			};
			cp.ErrorDataReceived += (sender, e) =>
			{
				// order of messages not guaranteed
				if (e.Data != null && e.Data.Trim().Length > 0) Console.Error.WriteLine("[CHILD] " + e.Data);
			};
			cp.OutputDataReceived += (sender, e) =>
			{
				// order of messages not guaranteed
				if (e.Data != null && e.Data.Trim().Length > 0) Console.WriteLine("[CHILD] " + e.Data);
			};

			try
			{
				assertChildProcessReady(cp);
				cp.Start();
				Thread.Sleep(1000); // @TODO https://bugzilla.xamarin.com/show_bug.cgi?id=44328
				cacheChildProcessInfo(cp);
				cp.BeginErrorReadLine();
				cp.BeginOutputReadLine();
				Console.WriteLine("CHILD PROCESS STARTED");
			}
			catch (Exception err)
			{
				Console.Error.WriteLine(String.Format(
					"Failed to boot CHILD: {0}",
					err.Message
				));
				Environment.Exit(1);
			}
			Console.WriteLine(cp.ToString());
		}

		static void assertChildProcessReady(Process cp)
		{
			if (File.Exists(cpInfoFilename))
			{
				JObject cpInfo = JObject.Parse(File.ReadAllText(cpInfoFilename));
				Process oldProcess;
				try
				{
					oldProcess = Process.GetProcessById(Int32.Parse((string)cpInfo["pid"]));
					if (oldProcess != null && ((string)cpInfo["filename"]).Contains("node"))
					{
						oldProcess.Kill();
						Thread.Sleep(1000);
					}
				}
				catch (ArgumentException err)
				{
					if (!err.Message.Contains("find process")) throw err;
				}
			}
		}

		static void cacheChildProcessInfo(Process cp)
		{
			JObject cpInfo = new JObject();
			cpInfo["pid"] = cp.Id;
			cpInfo["filename"] = cp.MainModule.FileVersionInfo.FileName; // @TODO field is incorrect without Thread.Sleep(1000)
			File.WriteAllText(
				cpInfoFilename,
				JsonConvert.SerializeObject(cpInfo)
			);
		}
	}
}
